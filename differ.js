/*
 * Copyright 2019 Magnus Hultin
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

const Serializer = require('./serializer');

class Differ {
  constructor({ ignore = [] } = {}) {
    this.ignore = ignore;
    this.serializer = new Serializer({ ignore });
  }

  diff(oldItems, newItems) {
    const oldMap = this.createMap(oldItems);
    const removedItems = [];
    const addedItems = newItems.filter((item) => !this.removeItemFromMap(item, oldMap));

    Object.values(oldMap).forEach((arr) => {
      removedItems.push(...arr);
    });

    return { remove: removedItems, add: addedItems };
  }

  createMap(items) {
    const map = {};

    items.forEach((item) => {
      const key = this.serializer.serialize(item);

      if (!map[key]) {
        map[key] = [];
      }

      map[key].push(item);
    });

    return map;
  }

  removeItemFromMap(item, map) {
    const key = this.serializer.serialize(item);

    if (map[key]) {
      if (map[key].length > 1) {
        // There's more than one item that serializes to the same key. Should
        // do something more intelligent.
        map[key].shift();

        return true;
      }

      delete map[key]; // eslint-disable-line no-param-reassign

      return true;
    }

    return false;
  }
}

module.exports = Differ;
