const { describe, it } = require('mocha');
const { expect } = require('chai');

const Serializer = require('../serializer');

const s = new Serializer();

describe('Serializer', () => {
  it('should serialize undefined', () => {
    expect(s.serialize(undefined)).to.equal('undefined');
  });

  it('should serialize null', () => {
    expect(s.serialize(null)).to.equal('NULL');
  });

  it('should serialize numbers', () => {
    expect(s.serialize(42)).to.equal('42');
    expect(s.serialize(3.14)).to.equal('3.14');
  });

  it('should serialize strings', () => {
    expect(s.serialize('Test string')).to.equal('"Test string"');
  });

  it('should serialize strings containing double quotes', () => {
    expect(s.serialize('Test "string"')).to.equal('"Test ""string"""');
  });

  it('should serialize arrays', () => {
    expect(s.serialize([])).to.equal('[]');
    expect(s.serialize([42])).to.equal('[42]');
    expect(s.serialize([42, 3.14, 'Test string'])).to.equal('[42,3.14,"Test string"]');
  });

  it('should serialize objects', () => {
    expect(s.serialize({})).to.equal('{}');
    expect(s.serialize({ everything: 42 })).to.equal('{everything:42}');
    expect(s.serialize({
      pi: 3.14,
      everything: 42,
      string: 'Test string',
    })).to.equal('{everything:42,pi:3.14,string:"Test string"}');
  });
});
