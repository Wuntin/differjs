const { describe, it } = require('mocha');
const { expect } = require('chai');
const Differ = require('../differ');
const Serializer = require('../serializer');

const d = new Differ({ ignore: ['id'] });
const s = new Serializer({ ignore: ['id'] });
const itemOne = {
  id: 1,
  value: 10,
};
const itemTwo = {
  id: 2,
  value: 10,
};
const differentItemOne = {
  id: 1,
  value: 12,
};
const diff = d.diff([itemOne], [itemTwo]);

describe('Ignore functionality', () => {
  it('should serialize to the same value if ignoring all differences', () => {
    expect(s.serialize(itemOne)).to.equal(s.serialize(itemTwo));
  });

  it('should serialize to different values when difference is not ignored', () => {
    expect(s.serialize(itemOne)).to.not.equal(s.serialize(differentItemOne));
  });

  it('should propagate from differ to its serializer', () => {
    expect(diff.remove).to.be.empty;
    expect(diff.add).to.be.empty;
  });
});
