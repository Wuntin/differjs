const { describe, it } = require('mocha');
const { expect } = require('chai');

const Serializer = require('../serializer');

const s = new Serializer();
const itemOne = {
  valueOne: 1,
  valueTwo: 2,
  valueThree: 3,
  valueFour: 4,
  valueFive: 5,
  valueSix: 6,
  valueSeven: 7,
  valueEight: 8,
  valueNine: 9,
  valueTen: 10,
};
const itemTwo = {
  valueTwo: 2,
  valueFour: 4,
  valueSix: 6,
  valueEight: 8,
  valueTen: 10,
  valueNine: 9,
  valueSeven: 7,
  valueFive: 5,
  valueThree: 3,
  valueOne: 1,
};

describe('Serializer: Items with multiple keys', () => {
  it('should serialize to the same value if equal', () => {
    expect(s.serialize(itemOne)).to.equal(s.serialize(itemTwo));
  });
});
