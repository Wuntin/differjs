const { describe, it } = require('mocha');
const { expect } = require('chai');

const Differ = require('../differ');

const d = new Differ();
const oldItems = [
  { id: 1 },
  { id: 2 },
];
const newItems = [
  { id: 1 },
  { id: 3 },
];

const diff = d.diff(oldItems, newItems);

describe('Differ: Basic functionality', () => {
  it('should not remove object that still exists', () => {
    expect(diff.remove).to.not.deep.include({ id: 1 });
  });

  it('should not add object that already existed', () => {
    expect(diff.add).to.not.deep.include({ id: 1 });
  });

  it('should remove object that disappeared', () => {
    expect(diff.remove).to.deep.include({ id: 2 });
  });

  it('should not add object that disappeared', () => {
    expect(diff.add).to.not.deep.include({ id: 2 });
  });

  it('should add object that appeared', () => {
    expect(diff.add).to.deep.include({ id: 3 });
  });

  it('should not remove object that appeared', () => {
    expect(diff.remove).to.not.deep.include({ id: 3 });
  });
});
