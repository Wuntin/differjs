const { describe, it } = require('mocha');
const { expect } = require('chai');
const Differ = require('../differ');

const d = new Differ();
const itemOne = {
  value: 10,
};
const itemTwo = {
  value: 20,
};
const diff = d.diff([itemOne, itemOne, itemTwo], [itemOne, itemTwo, itemTwo]);

describe('Duplicate items', () => {
  it('should remove an item when old set contains one extra', () => {
    expect(diff.remove).to.deep.include(itemOne);
    expect(diff.add).to.not.deep.include(itemOne);
  });

  it('should add an item when new set contains one extra', () => {
    expect(diff.remove).to.not.deep.include(itemTwo);
    expect(diff.add).to.deep.include(itemTwo);
  });
});
