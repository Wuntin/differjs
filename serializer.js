/*
 * Copyright 2019 Magnus Hultin
 *
 * This library is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published
 * by the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

class Serializer {
  constructor({ ignore = [] } = {}) {
    this.ignore = ignore;
  }

  serialize(item) {
    if (item === null) { return 'NULL'; }

    if (typeof item === 'object') {
      if (Array.isArray(item)) {
        return this.serializeArray(item);
      }

      return this.serializeObject(item);
    }

    if (typeof item === 'string') {
      return `"${item.replace(/"/g, '""')}"`;
    }

    return String(item);
  }

  serializeArray(item) {
    const str = item.map((subItem) => this.serialize(subItem)).join(',');

    return `[${str}]`;
  }

  serializeObject(item) {
    const keys = Object.keys(item).filter(
      (key) => item[key] != null && !this.ignore.includes(key) // eslint-disable-line comma-dangle
    );
    const str = keys.sort().map((key) => `${key}:${this.serialize(item[key])}`).join(',');

    return `{${str}}`;
  }
}

module.exports = Serializer;
