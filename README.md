# differjs

## SYNOPSIS

  ```
  // Load module.
  const Differ = require('differ');

  // Create an instance that will ignore properties named "id"
  // when comparing objects.
  const differ = new Differ({ ignore: ['id'] });

  // Compare two sets of objects.
  const diff = differ.diff(oldSet, newSet);

  // diff.remove is a list of objects that were removed.
  // diff.add is a list of objects that were added.
  ```
